import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Imman Ella";
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: "Sumire",
//   lastName: "Yoshizawa"
// };

// function formatName(user) {
//   return user.firstName + " " + user.lastName;
// };

// const element = <h1>Hewwo, {formatName(user)}</h1>

// root.render(element);