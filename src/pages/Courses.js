import {useState, useEffect} from 'react';

// import coursesData from '../data/coursesData';
import CourseCard from "../components/CourseCard";

export default function Courses() {
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// Retrives the courses from the database upon initial render of the "Courses" component
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all-active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course={course} />
	// 	)
	// });

	return (
		<>
		{courses}
		{/* Props Drilling - we can pass information from one component to another using props */}
		{/* {} - used in props to signify that we are providing information */}
		{/*<CourseCard courseProp = {coursesData[0]} />*/}
		</>
	)
}